# README #

This repo is for demonstration boards.

### Structure ###

DemonstrationBoards
-> BOARD_FOLDER
--> Gerbers
--> Schematics
--> Arduino Code
--> PCB Files
--> Documentation (on Confluence first and extra here)
--> Datasheets
-> Readme

### Who do I talk to? ###

* Declan Walsh
* Esther Chong/William Chen (Collision_Detector)
* Harry Price (Spin Detector)
* Yuchen Wang 