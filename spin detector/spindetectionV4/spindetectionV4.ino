// SDA->A4
// SCL->A5
#include<Wire.h>
#include <LiquidCrystal_I2C.h>

#define GYRO_CONFIG 0x1B
#define GYRO_FS_250 0
#define GYRO_FS_500 8
#define GYRO_FS_1000  16
#define GYRO_FS_2000  24

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

const int MPU_addr=0x68;  // I2C address of the MPU-6050
long GyX,GyY,GyZ;
long multiplier = 500; // max value of 500 degrees per second
long range = 32767;

void setup(){
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);

  Wire.beginTransmission(MPU_addr);
  Wire.write(GYRO_CONFIG);
  Wire.write(GYRO_FS_500);    //set the full scale value to 500 degrees per second.
  Wire.endTransmission(true);

  lcd.begin(16,2);
}

void loop(){
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x43);  // starting with register 0x43 (GYRO_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,6,true);  // request a total of 6 registers

  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  GyX = GyX + 180; //normalising all readings to 0
  GyY = GyY - 20;
  GyZ = GyZ - 95;

  GyX = (GyX*multiplier)/range;
  GyY = (GyY*multiplier)/range;
  GyZ = (GyZ*multiplier)/range;
 
  lcd.setCursor(2,0);
  lcd.print("      ");
  lcd.setCursor(0,0); 
  lcd.print("X=");
  lcd.setCursor(2,0);
  lcd.print(GyX);

  lcd.setCursor(10,0);
  lcd.print("      ");
  lcd.setCursor(8,0); 
  lcd.print("Y=");
  lcd.setCursor(10,0);
  lcd.print(GyY);

  lcd.setCursor(2,1);
  lcd.print("      ");
  lcd.setCursor(0,1); 
  lcd.print("Z=");
  lcd.setCursor(2,1);
  lcd.print(GyZ);
  
  delay(333);
}
